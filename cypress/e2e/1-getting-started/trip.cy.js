describe('Search Paris-Bruxelles', () => {
  it('Search Paris-Bruxelles', () => {
    cy.visit('../../index.html')
    cy.get('#departure').type('Paris')
    cy.get('#arrival').type('Bruxelles')
    cy.get('#date').type('2024-03-25')
    cy.get('#search').click()
    cy.get('#results').should('contain', 'Paris > Bruxelles')
  })
})


describe('Save to my cart', () => {
  it('Save to my cart', () => {
    cy.visit('../../index.html')

    /* ==== Generated with Cypress Studio ==== */
    cy.get('#departure').clear('P');
    cy.get('#departure').type('Paris');
    cy.get('#arrival').clear('B');
    cy.get('#arrival').type('Bruxelles');
    cy.get('#date').clear('2024-03-25');
    cy.get('#date').type('2024-03-25');
    cy.get('#card').click();
    cy.get('#search').click();
    cy.get('#\\36 6032af2bb75c751044addf4').click();
    cy.get('#title').click();
    cy.get('[href="cart.html"]').click();
    cy.get('.selected-trip').should('contain', 'Paris > Bruxelles');
    /* ==== End Cypress Studio ==== */
  })
})

describe('Delete element from my cart', () => {
  it('Delete element from my cart', () => {
    cy.visit('../../index.html')

    /* ==== Generated with Cypress Studio ==== */
    cy.get('#departure').clear('P');
    cy.get('#departure').type('Paris');
    cy.get('#arrival').clear('B');
    cy.get('#departure').click();
    cy.get('#departure').clear('B');
    cy.get('#departure').type('Bruxelles');
    cy.get('#arrival').clear();
    cy.get('#arrival').type('Lyon');
    cy.get('#date').clear('2024-03-02');
    cy.get('#date').type('2024-03-25');
    cy.get('#search').click();
    cy.get('#\\36 6032af2bb75c751044addee').click();
    cy.get('#\\36 6032af2bb75c751044addee').click();
    cy.get('#container').should('not.contain', 'Bruxelles > Lyon');
    /* ==== End Cypress Studio ==== */
  })

  it('Purchase elements from my cart', () => {
    cy.visit('../../index.html')

    /* ==== Generated with Cypress Studio ==== */
    cy.get('[href="bookings.html"]').click();
    cy.get('[href="cart.html"]').click();
    cy.get('#purchase').click();
    cy.get('#trips').should('contain', 'Paris > Bruxelles');
    /* ==== End Cypress Studio ==== */
  })
})
